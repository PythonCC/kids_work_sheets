# Kids Work Sheets


## Technologies Used

- **Python**: The core programming language used for development.
- **Poetry**: Dependency management tool for Python projects.
- **[python-docx-template (docxtpl)](https://docxtpl.readthedocs.io/en/latest/)**: Library for generating Word 
  documents with Python.
- **Jinja2**: Templating engine used by docxtpl.


## Overview

This is a Python tool that generates basic math problems and word-to-picture matching exercises for children. It was 
developed to engage children in educational activities and enhance their numerical skills and vocabulary. Leveraging Python and various libraries, the application automates the creation of these activities, providing an easy-to-use solution for parents and educators.

## Word-to-Picture Matching

This program is quite simple it uses os to get the list of images available in the images folder, scrambles them up 
and uses the file name as the word for the exercise.

See /templates/word_match.docx for the template used,

## Installation

To install Printable Activity Generator and its dependencies, you can use Poetry:

```bash
$ poetry install
```

## Usage

```bash
$ poetry run python word_match.py
```

```bash
$ poetry run python math_problems.py
```

These will generate a new docx file in the currnet directory.

## Contributing

Contributions are welcome! If you'd like to contribute to Printable Activity Generator, please follow these steps:

1. Fork the repository
2. Create your feature branch (`git checkout -b feature`)
3. Make your changes
4. Commit your changes (`git commit -am 'Add feature'`)
5. Push to the branch (`git push origin feature`)
6. Create a new Pull Request

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
