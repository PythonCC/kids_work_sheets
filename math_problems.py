import random

from docxtpl import DocxTemplate


def gen_problem(symbol=None):
    operators = [
        "+",
        "-",
        # "*", "/"
    ]
    operator = random.choice(operators)

    if operator == "-":
        a = random.randint(2, 5)
        b = random.randint(1, a - 1)
    else:
        a = random.randint(1, 5)
        b = random.randint(1, 5)

    hint = random.random() < 0.6
    show = hint and random.random() > 0.6
    show_symbol = hint and not show
    return {
        "left": symbol * a if symbol else a,
        "right": symbol * b if symbol else b,
        "answer": (a + b) if operator == "+" else (a - b),
        "answer_symbol": (
            ((a + b) if operator == "+" else (a - b)) * symbol if symbol else ""
        ),
        "show": show,
        "show_symbol": show_symbol,
        "operator": operator,
    }


symbols = (
    # "■",
    "□",
    # "◆",
    "◇",
    # "▲",
    "△",
    # "▼",
    "▽",
    "●",
    "○",
    "★",
    "☆",
    # "♠",
    # "♣",
    "♥",
    "♦",
    "☀",
    # "☃",
    # checkmark
    "✓",
    "✗",
    "◖",
    "◗",
    # "☘",
    # "☠",
    # "☢",
    # "☣",
    # "☥",
    "☯",
    "☹",
    "☺",
    "☼",
    "☾",
)

doc = DocxTemplate("templates/math_problems.docx")

context = {"rows": [gen_problem(random.choice(symbols)) for _ in range(24)]}

doc.render(context)

doc.save("math_problems.docx")
