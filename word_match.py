import os
import random

from docx.shared import Mm
from docxtpl import DocxTemplate, InlineImage

image_dir = "img/animals"
animals = [f for f in os.listdir(image_dir) if f.endswith(".jpg") or f.endswith(".png")]

images = [*animals]

random.shuffle(images)
images = images[:8]

doc = DocxTemplate("templates/word_match.docx")

print(images)

inline_images = [
    InlineImage(doc, image_descriptor=f"{image_dir}/{f}", width=Mm(30), height=Mm(30))
    for f in images
]
words = [
    random.choice(
        [getattr(f.split(".")[0], func)() for func in ["title", "upper", "lower"]]
    )
    for f in images
]

random.shuffle(words)
random.shuffle(inline_images)

context = {
    "word_matches": [
        {"name": word, "image": image} for image, word in zip(inline_images, words)
    ]
}
doc.render(context)

doc.save("word_match.docx")
